def make_answer_dict(text, options=None, image=None):
    answer_dict = {'text': text}
    if options:
        answer_dict['options'] = options
    if image:
        answer_dict['image'] = image
    return answer_dict


class Question:
    next_question_id: int = 0

    def __init__(self, questions_dict: dict, current_answer=None, current_question_id=0):
        self.current_answer = current_answer
        self.current_question_id = current_question_id
        self.questions_dict = questions_dict
        self.current_question = self.find_question(self.current_question_id)
        self.next_question_id = current_question_id

    def answer(self) -> dict:
        if self.process_question():
            return self.next_question()
        elif 'hint' in self.current_question:
            return make_answer_dict(self.current_question['hint'])
        else:
            return make_answer_dict('No :(')  # TODO make configurable

    def find_question(self, question_id):
        if question_id < len(self.questions_dict['questions']):
            return self.questions_dict['questions'][question_id]
        else:
            return None

    def start(self):
        question = self.find_question(0)
        return question['q']

    def next_question(self) -> dict:
        self.next_question_id = self.current_question_id + 1
        next_question = self.find_question(self.next_question_id)
        if not next_question:
            return make_answer_dict('END')  # TODO make configurable
        next_question_dict = make_answer_dict(next_question['q'])
        if 'o' in next_question:
            next_question_dict['options'] = next_question['o']
        if 'image' in next_question:
            next_question_dict['image'] = next_question['image']
        if 'gif' in next_question:
            next_question_dict['gif'] = next_question['gif']
        return next_question_dict

    def process_question(self) -> bool:
        if 'a' not in self.current_question:
            return True
        return self.current_answer == str(self.current_question['a'])
