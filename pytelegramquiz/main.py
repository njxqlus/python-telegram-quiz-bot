import os
import sys
import logging
import time

from telegram import Bot, ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from dotenv import load_dotenv
from yaml import load, SafeLoader

from pytelegramquiz.question_class import Question

# Load env variables
load_dotenv()
telegram_bot_token = os.getenv('TOKEN')

# Init bot
bot = Bot(token=telegram_bot_token)

print(bot.getMe())

# Init updater and dispatcher
updater = Updater(token=telegram_bot_token, use_context=True)
dispatcher = updater.dispatcher

# Start receiving updates from bot
updater.start_polling()

# Init logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO, stream=sys.stdout)

# Chats dictionary
chats = {}

# Load quiz dictionary
stream = open('../questions.yaml', 'r')
quiz_dict = load(stream, SafeLoader)


def start(update, context):
    chat_id = update.effective_chat.id

    global chats
    current_question_id = chats.get(chat_id, None)
    if not current_question_id:
        logging.info(f"This is new chat {chat_id}, starting quiz")
        context.bot.send_message(chat_id=chat_id, text=os.getenv('HELLO_MESSAGE', 'HELLO!'))

        global quiz_dict
        question = Question(questions_dict=quiz_dict)
        question_text = question.start()
        time.sleep(1)
        logging.info('Bot sent: ' + question_text)
        context.bot.send_message(chat_id=chat_id, text=question_text)
        chats[chat_id] = 0
        logging.info('Current id: ' + str(chats[chat_id]))
    # TODO make restart


def echo(update, context):
    logging.info('Incoming message: ' + update.message.text)

    # Get question id for current chat
    global chats
    chat_id = update.effective_chat.id
    current_question_id = chats.get(chat_id, 0)
    logging.info('Current id: ' + str(current_question_id))

    # Find answer for question
    global quiz_dict
    question = Question(questions_dict=quiz_dict,
                        current_answer=update.message.text, current_question_id=current_question_id)
    message = question.answer()

    if 'options' in message:
        keyboard = list()
        keyboard.append(message.get('options'))
        reply_markup = ReplyKeyboardMarkup(keyboard)
    else:
        reply_markup = ReplyKeyboardRemove()

    context.bot.send_message(chat_id=chat_id, text=message.get('text'), reply_markup=reply_markup)

    if 'image' in message:
        image = open('../' + message.get('image'), 'rb')
        time.sleep(1)
        context.bot.send_photo(chat_id=chat_id, photo=image)
        logging.info('Bot sent image')

    if 'gif' in message:
        gif = open('../' + message.get('gif'), 'rb')
        time.sleep(1)
        bot.send_animation(chat_id=chat_id, animation=gif)
        logging.info('Bot sent gif')

    logging.info('Bot sent: ' + message.get('text'))
    chats[update.effective_chat.id] = question.next_question_id
    logging.info('Next id: ' + str(chats[chat_id]))


# Add handler for /start command
start_handler = CommandHandler('start', start)
dispatcher.add_handler(start_handler)

# Add handler for message
echo_handler = MessageHandler(Filters.text & (~Filters.command), echo)
dispatcher.add_handler(echo_handler)
